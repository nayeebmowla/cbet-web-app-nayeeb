import { CbetWebAppPage } from './app.po';

describe('cbet-web-app App', () => {
  let page: CbetWebAppPage;

  beforeEach(() => {
    page = new CbetWebAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
